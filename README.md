# energy-go-front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Icons
* Icons are transformed to font with icomoon app.
* Import them then transform them to fonts.
* Copy past the css to _fonts.scss