import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */'@/views/Home')
  },
  {
    path: '/project',
    step: 1,
    name: 'Mon projet solaire',
    component: () => import(/* webpackChunkName: "index" */'@/views/partial/_index'),
    children: [
      {
        path: 'production',
        component: () => import(/* webpackChunkName: "index" */'@/views/partial/_index'),
        children: [
          {
            path: '',
            name: 'Production',
            component: () => import(/* webpackChunkName: "production" */'@/views/project/Production')
          },
          {
            path: 'help',
            name: 'ProductionHelp',
            component: () => import(/* webpackChunkName: "production-help" */'@/views/project/ProductionHelp')
          },
          {
            path: 'hint',
            name: 'ProductionHint',
            component: () => import(/* webpackChunkName: "production-hint" */'@/views/project/ProductionHint')
          }
        ]
      },
      {
        path: 'equipment',
        component: () => import(/* webpackChunkName: "index" */'@/views/partial/_index'),
        children: [
          {
            path: '',
            name: 'Equipment',
            component: () => import(/* webpackChunkName: "equipment" */'@/views/project/Equipment')
          },
          {
            path: 'help',
            name: 'EquipmentHelp',
            component: () => import(/* webpackChunkName: "equipment-help" */'@/views/project/EquipmentHelp')
          },
          {
            path: 'hint',
            name: 'EquipmentHint',
            component: () => import(/* webpackChunkName: "equipment-hint" */'@/views/project/EquipmentHint')
          }
        ]
      },
    ]
  },
  {
    path: '/house',
    step: 2,
    name: 'Ma maison',
    component: () => import('@/views/partial/_index'),
    children: [
      {
        path: 'height',
        name: 'Height',
        component: () => import(/* webpackChunkName: "height" */'@/views/house/Height')
      },
      {
        path: 'tilt',
        name: 'Tilt',
        component: () => import(/* webpackChunkName: "tilt" */'@/views/house/Tilt')
      },
      {
        path: 'cover',
        name: 'Cover',
        component: () => import(/* webpackChunkName: "cover" */'@/views/house/Cover')
      },
      {
        path: 'panel',
        name: 'BuildingPanel',
        component: () => import(/* webpackChunkName: "building-panel" */'@/views/house/Panel')
      },
    ]
  },
  {
    path: '/address',
    step: 3,
    name: 'Mon adresse',
    component: () => import('@/views/partial/_index'),
    children: [
      {
        path: 'street',
        name: 'Street',
        component: () => import(/* webpackChunkName: "street" */'@/views/address/Street')
      },
      {
        path: 'panel',
        name: 'Panel',
        component: () => import(/* webpackChunkName: "panel" */'@/views/address/Panel')
      },
    ]
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import(/* webpackChunkName: "contact" */'@/views/Contact')
  },
  {
    path: '/final',
    name: 'Final',
    component: () => import(/* webpackChunkName: "final" */'@/views/Final')
  },
  {
    path: '/components',
    name: 'Components',
    component: () => import(/* webpackChunkName: "components" */'@/views/Components')
  },
  {
    path: '*',
    name: 'PageNotFound',
    component: () => import(/* webpackChunkName: "not-found" */'@/views/PageNotFound')
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return {x: 0, y: 0}
  },
  routes
})

export default router
