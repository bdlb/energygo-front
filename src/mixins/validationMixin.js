export default {
  data () {
    return {
      valid: false
    }
  },
  computed: {
    formRefName () {
      return this.$options._componentTag + 'Form'
    }
  },
  methods: {
    validateFormAndRedirect (routeName) {
      if (this.validate()) {
        this.next(routeName)
      }
    },
    validateFormAndCallApi (endpoint, data) {
      if (this.validate()) {
        alert(endpoint + " " + data)
      }
    },
    validate() {
      return this.$refs[this.formRefName].validate()
    }
  }
}