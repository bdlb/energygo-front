export default {
  data () {
    return {
      rules: {
        required: value => (!!value || value === false || value === 0) || 'Ce champ est requis',
        email: value => {
          const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          return pattern.test(value) || "Veuillez renseigner une adresse email valide."
        },
      }
    }
  }
}