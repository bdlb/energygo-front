import {mapActions} from "vuex";

export default {
  methods: {
    next(routeName) {
      this.$router.push({name: routeName})
    },
    ...mapActions({
      updateData: "updateData"
    }),
  }
}