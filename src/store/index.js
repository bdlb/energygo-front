import Vue from 'vue'
import Vuex from 'vuex'
import {merge} from "lodash"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: {
      production: null,
      power: null,
      buildingHeight: null,
      buildingCover: "",
      buildingTilt: null,
      buildingPanel: null,
      address: "",
      contact: {
        firstName: "",
        lastName: "",
        mail: "",
        phone: "",
        callback: {
          day: null,
          hour: null
        },
        knew: null
      }
    }
  },
  getters: {
    getData: state => state.data,
    getProduction: state => state.data.production,
    getPower: state => state.data.power,
    getBuildingHeight: state => state.data.buildingHeight,
    getBuildingCover: state => state.data.buildingCover,
    getBuildingTilt: state => state.data.buildingTilt,
    getBuildingPanel: state => state.data.buildingPanel,
    getAddress: state => state.data.address,
    getContact: state => state.data.contact
  },
  mutations: {
    updateData(state, data) {
      merge(state.data, data)
    }
  },
  actions: {
    updateData(context, data) {
      context.commit('updateData', data)
    }
  },
  modules: {
  }
})
